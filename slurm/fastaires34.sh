#!/bin/bash
#SBATCH --nodes 1
#SBATCH -J res34
#SBATCH -o res34results
#SBATCH --partition main
#SBATCH --time 24:00:00

source ./venv/bin/activate
python3 ./scripts/fastaires34.py