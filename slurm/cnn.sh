#!/bin/bash
#SBATCH --nodes 1
#SBATCH -J cnn
#SBATCH -o cnnresults
#SBATCH --partition main
#SBATCH --time 24:00:00

source ./venv/bin/activate
python3 ./scripts/cnn.py