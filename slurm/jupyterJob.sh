#!/bin/bash
#SBATCH --nodes 1
#SBATCH --partition gpu
#SBATCH --time 24:00:00
#SBATCH --mem 10000
#SBATCH --gres=gpu:tesla:1

module load python-3.6.0
jupyter-start