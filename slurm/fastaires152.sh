#!/bin/bash
#SBATCH --nodes 1
#SBATCH -J res152
#SBATCH -o res152results
#SBATCH --partition main
#SBATCH --time 24:00:00

source ./venv/bin/activate
python3 ./scripts/fastaires152.py