#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#import torch
#torch.cuda.is_available()


# In[1]:


from fastai import *
from fastai.metrics import error_rate, accuracy
from fastai.data.all import *
from fastai.vision.all import *


# In[2]:


Data = DataBlock( blocks=(ImageBlock, CategoryBlock), get_items=get_image_files, 
splitter=RandomSplitter(valid_pct=0.2, seed=42), get_y=parent_label)
dls = Data.dataloaders("./scripts/TartuNet2/train")


# In[3]:


#dls.show_batch()


# In[4]:


learn = cnn_learner(dls, resnet152, metrics=[accuracy, error_rate])
learn.fit_one_cycle(3)


# In[ ]:


learn.export("fastai_res152.model")

