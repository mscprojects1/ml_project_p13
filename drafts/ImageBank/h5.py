import h5py
import numpy as np
from PIL import Image
from pathlib import Path
from PIL.ExifTags import TAGS
import json

def store_single_hdf5(image, image_id, label):
    """ Stores a single image to an HDF5 file.
        Parameters:
        ---------------
        image       image array, (32, 32, 3) to be stored
        image_id    integer unique ID for image
        label       image label
    """
    # Create a new HDF5 file
    #file = h5py.File(hdf5_dir / f"{image_id}.h5", "w")
    file = h5py.File("./images" / "222.h5", "w")

    # Create a dataset in the file
    dataset = file.create_dataset("image", np.shape(image), h5py.h5t.STD_U8BE, data=image)
    meta_set = file.create_dataset("meta", np.shape(label), h5py.h5t.STD_U8BE, data=label)
    file.close()

im = Image.open(Path("../LVpildid/TEEMAD/Inimesed/Pered/IMG_3728.jpg"))
print(im.getdata())
exifdata = im.getexif()
print(exifdata )
# iterating over all EXIF data fields
dct = {}
for tag_id in exifdata:
    # get the tag name, instead of human unreadable tag id
    tag = TAGS.get(tag_id, tag_id)
    data = exifdata.get(tag_id)
    # decode bytes
    if isinstance(data, bytes):
        data = data.decode()


    dct[tag] = data

    print(f"{tag:35}: {data}")

print(dct)
print(json.load(dct))
print(im)