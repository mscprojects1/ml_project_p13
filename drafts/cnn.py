#!/usr/bin/env python
# coding: utf-8

# In[2]:


from keras.models import Sequential
from keras.layers import Conv2D, MaxPool2D, Flatten, Dense, InputLayer, BatchNormalization, Dropout
from keras.layers import Input, Conv2D, Activation, Flatten, Dense, MaxPooling2D, BatchNormalization, Dropout
import os
from keras.preprocessing.image import ImageDataGenerator


# In[10]:


#import torch
#torch.cuda.is_available()


# In[1]:


def writetofile(filename, text):
    with open(filename, 'a', encoding='utf-8') as f:
        f.write(text)


def rowbyrow(filename):
    with open(filename, encoding='utf-8') as f:
        content = f.readlines()
    for i in range(len(content)):
        content[i] = content[i].strip()
    return content

def readfull(filename):
    with open(filename, encoding='utf-8') as f:
        content = f.read()
    return content


# In[3]:


labels = rowbyrow("./scripts/TartuNet2/labels.txt")


# In[5]:


len(labels)


# In[9]:


batch_size = 32
nb_epochs = 30

imagegen = ImageDataGenerator(validation_split=0.2)

train = imagegen.flow_from_directory(directory="./scripts/TartuNet2/train/", 
                                     class_mode="categorical", 
                                     shuffle=True, batch_size=batch_size,
                                     subset="training",
                                     target_size=(387, 469))


validation_dataset = imagegen.flow_from_directory(batch_size=batch_size,
                                                 directory="./scripts/TartuNet2/train/",
                                                 shuffle=True,
                                                 target_size=(387, 469), 
                                                 subset="validation",
                                                 class_mode='categorical')


# In[11]:



model = Sequential()
model.add(InputLayer(input_shape=(387, 469, 3)))

# 1st conv block
model.add(Conv2D(25, (5, 5), activation='relu', strides=(1, 1), padding='same'))
model.add(MaxPool2D(pool_size=(2, 2), padding='same'))
# 2nd conv block
model.add(Conv2D(50, (5, 5), activation='relu', strides=(2, 2), padding='same'))
model.add(MaxPool2D(pool_size=(2, 2), padding='same'))
model.add(BatchNormalization())
# 3rd conv block
model.add(Conv2D(70, (3, 3), activation='relu', strides=(2, 2), padding='same'))
model.add(MaxPool2D(pool_size=(2, 2), padding='valid'))
model.add(BatchNormalization())
# ANN block
model.add(Flatten())
model.add(Dense(units=100, activation='relu'))
model.add(Dense(units=100, activation='relu'))
model.add(Dropout(0.25))
# output layer
model.add(Dense(units=38, activation='softmax'))

# compile model
model.compile(loss='categorical_crossentropy', optimizer="adam", metrics=['accuracy'])
# fit on data for 30 epochs
model.fit_generator(train, epochs=30)

hist = model.fit_generator(
    train_generator,
    steps_per_epoch = train_generator.samples // batch_size,
    validation_data = validation_generator, 
    validation_steps = validation_generator.samples // batch_size,
    epochs = nb_epochs)


# In[ ]:


model.save('kerasCNN.model')


# In[ ]:


writetofile('kerasCNNhist', hist)

