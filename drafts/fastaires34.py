#!/usr/bin/env python
# coding: utf-8

# In[1]:


#import torch
#torch.cuda.is_available()


# In[14]:


from fastai import *
from fastai.metrics import error_rate, accuracy
from fastai.data.all import *
from fastai.vision.all import *


# In[5]:


#data= ImageDataLoaders.from_folder("./TartuNet2/train",train = "train", valid_pct=0.2)


# In[15]:


Data = DataBlock( blocks=(ImageBlock, CategoryBlock), get_items=get_image_files, 
splitter=RandomSplitter(valid_pct=0.2, seed=42), get_y=parent_label)
dls = Data.dataloaders("./scripts/TartuNet2/train")


# In[18]:


#dls.show_batch()


# In[ ]:


learn = cnn_learner(dls, resnet34, metrics=[accuracy, error_rate])
learn.fit_one_cycle(3)


# In[ ]:


learn.export("fastai_res34.model")

