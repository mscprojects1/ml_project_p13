This is a project for Machine Learning course at University of Tartu.

Team members: Kairit Peekman, Tõnis Hendrik Hlebnikov, Kaire Koljal

contact: kairit.peekman@gmail.com, kaire.koljal@gmail.com

[[_TOC_]]

# Labeling Tartu City Image Bank

## 1. Description

So far the imagebank has been managed manually. It consists of two main folders: "Teemad" (Subjects) and "Sündmused" (Events).

The "Sündmused" folder contains images of different events that the City Government has participated in or organized.

The "Teemad" folder has topic based folders that we use as manually annotated labels. Usually the images are copied from "Sündmused" folder into a suitable topic folder.

![](readmeImages/1.jpg)

The "Sündmused" folder has 51671 images (274 GB)

The "Teemad" folder has 6985 images (49GB)

In total there are 58657 images (322GB)

The images vary in size, file extension, and sometimes the file names include symbols that the system could not read.

For us, the biggest challenge was the large volume of images and the time that it took to process them. It is difficult to find a working model, since all th eprocesses took a long time. In addition, the image bank is out of balance in terms of labels. Some labels have a lot of images and some have less.

We set two goals for ourselves for this project:
1) clean up the image bank so there are not duplicate images and the images would be searchable
2) label the images that hadn't been given labels manually, based on the system that the other images were labeled

None of these goals were fulfilled as well as we would have liked because testing out several solutions took longer than we had anticipated. Nevertheless, we went through several stages during the project, and we reached some results.

## 2. Detecting labels

Manually added labels were usually located in the image or folder name. So we had to pick out possible labels. We defined a keyword as label if the image content matched the keyword. This meant that we separated them from objective factors such as: car, date, place, specific event.
In these cases, these are things that do not require model identification. In many cases, they are known in some way. Thus, the label had to represent something about what was happening in the picture, not the everyday objects in it.
Since the images must remain searchable according to all these things, we must also keep this data. This means that there is a lot of data associated with the image in addition to the labels.

We reviewed the labels we used and generalized them. We initially got 43 labels, but there were only 38 left because some didn't have enough pictures and one had too many pictures. Namely, we introduced the label "üritus" ("event"), but then it became clear that almost everything can be classified as an event, and there is no point in distinguishing it.

The final labels are as follows:
1. aastavahetus
2. annelinn
3. autovabadus
4. bussid
5. eakad
6. ehitus
7. emajõgi
8. istutamine
9. jõulud
10. kaarsild
11. keskkond
12. kevad
13. kirik
14. kliinikum
15. kodu
16. koer
17. kool
18. kunst
19. lasteaed
20. laulupidu
21. linnaruum
22. maskid
23. mänguväljak
24. noored
25. raekoda
26. rattalinn
27. raudtee
28. sild
29. sotsiaalmeedia
30. sport
31. supilinn
32. suvi
33. sügis
34. sümboolika
35. talv
36. tantsupidu
37. toit
38. ülikool


## 3. Collecting image data

Once we got the labels, we could start collecting data from the images and adding labels. We initially collected the data for all the images' data in a file.

We also tried a database like ZODB. We read all the data of all the images as an object into this database. We still have them in the database in such format. The disadvantage of this database is that it is very space consuming and therefore not efficient. Also, searching images from there is not effective. However, it contains aöö the important data for all images:

![](readmeImages/2.jpg)

The difficult part about annotating with labels was that due to the large volume of images, we could not test the effectiveness of our annotation. When we were done, it turned out that adding labels should have been more conservative. Our solution was to look for label related words from yhe image path. We created a dictionary that had possible words related to the label. In addition, we excluded the authors' name as a word. The result was somewhat confusing:

![](readmeImages/3.jpg)

These are not the worst examples, but it also shows that some labels might not be correct. Some images could have multiple labels.

![](readmeImages/4.jpg)

The number of images per labels is as follows:

![](readmeImages/osa3labels.jpg)

We removed the "events" label since it was so large, but we didn't notice that the label "linnaruum" had the same problem and it was left into the traing set. The effect of that we can see later in the part where we show examples of how the model predicted.

The labels that were exluded completely were "mehed", "vanemuine", "üritused". We should have left out "linnaruum" as well since it is quite general like "üritused".

At least 2 other processes took place in parallel with the data collection from the images. First, we detected duplicates. To do this, we used the solution proposed by Tõnis, where each image received a hash code, which was unique to it depending on the pixel matrix of that image, but if there was another similar image, it was the same. So we identified images that were duplicates.

There were 49376 unique pictures in the picture bank.

Secondly, in parallel with data collection, we also performed image pre-processing. We compressed the images to one size and one format (.jpg). The processed images were renamed to have the hash as name. We made a csv file of the image labels, names and paths. To speed up further processing and implement different models, we gave the processed image bank format similar to ImageNet. We wanted to use different models more easily and as quickly as possible, because even though we used the university HPC, all activities were very time consuming.

![](readmeImages/3_a.jpg)

## 4. Training models

We did several experiments with training. In the beginning, when we didn't have a data set in ImageNet-like format and the preprocessing of the images took place on an ongoing basis with each model, the training was very slow. Errors also occurred because the image bank contained files that could not be read by existing resources. For example, one specific problem was the "õ" letter in the file name, but there were also formats such as .tiff, etc. that were not supported by the tools. So there were a lot of interruptions.

As a result, we realized that it makes sense to process images once and keep them processed so that you can apply different models faster and try different solutions.

We considered different options, but since Fastai seemed to be the most effective in classifying the image in the practice sessions, we decided to give it a try. The downside of Fastai is that it is difficult to understand and get started.

At first, when Fastai had to process the images every time it made a model, it also seemed slow, but when it got the pre-processed images it was actually very fast. To make sure of this, we also made a Keras Sequential model. In 15 hours the model trained only this much:

![](readmeImages/5.jpg)

Next, let's look at the models we used. We used some different Fastai pre-trained models: resnet 34, resnet 50, resnet 152, VGG16_bn. In addition, we made two identical models ourselves: one as a Kerase Sequential model and the other as a similar Fastai model.

We read the background information about the models used through Fastai here: https://arxiv.org/pdf/2002.08991.pdf

### 4.1 Resnet 34

Resnet 34 remained as one model, because we also used it in the practice session. However, our own experience showed that it had the best cost-benefit ratio. The heavier models took considerably more time, but the result was not significantly different. Since time was the most critical for us due to the slowness of the processes, Resnet 34 fully justified itself in this sense.

#### 4.1.1 Epoch 3, batch size 64

The very first experiment was with a relatively small number of epochs. We later learned that a larger number means more time, but doesn't add much. Thus, the baseline with 3 epochs was actually good enough in our current circumstances, where we actually should have processed the training data labels because they did not provide very good input.

Training data:

![](readmeImages/6.jpg)

![](readmeImages/7.jpg)

This was the only model that we tried to fine-tune, which did not make the results much better.

![](readmeImages/8.jpg)

Some examples of the results:

![](readmeImages/9.jpg)

![](readmeImages/10.jpg)

![](readmeImages/11.jpg)

#### 4.1.2 Epoch 10, batch size 64

Training data:

![](readmeImages/12.jpg)

We can see that the model overfitted with 10 epochs. 5 epochs would have been the correct number. At the same time, the labels were not always trustworthy or the image could be interpreted in multiple ways. Therefore, the accuracy can't be fully trusted.

Learning:

![](readmeImages/13.jpg)

Some examples:

![](readmeImages/14.jpg)

![](readmeImages/15.jpg)

![](readmeImages/16.jpg)


#### 4.1.3 Epoch 3, batch size 128

Training data:

![](readmeImages/17.jpg)

Learning:

![](readmeImages/18.jpg)

Some examples:

![](readmeImages/19.jpg)

![](readmeImages/20.jpg)

![](readmeImages/21.jpg)

### 4.2 Resnet 50 (epoch 3, batch size 64)

We assumed that Resnet 50 would be more powerful as pre-trained model. The results were similar to Resnet 34.

Trainign data:

![](readmeImages/22.jpg)

Learning:

![](readmeImages/23.jpg)

Some examples from trainig:

![](readmeImages/24.jpg)

![](readmeImages/25.jpg)

![](readmeImages/26.jpg)

The worst 9 results:

![](readmeImages/27.jpg)

We can see that some of the predictions could be as it predicted. We need to take into account that pur manually added labels were not very precise.

### 4.3 Resnet 152 (epoch 3, batch size 32)

Due to constant problems of being out of memoryt, this large model could not use the 64 batch size. The maximum it could use in our conditions was 32. We would have liked to have kept this parameter for every model, but this was not possible for larger models.

Training data:

![](readmeImages/28.jpg)

Learning:

![](readmeImages/29.jpg)


Some examples:

![](readmeImages/30.jpg)

![](readmeImages/31.jpg)

![](readmeImages/32.jpg)

### 4.4 VGG16_bn (epoch 3, batch size 64)

TRainign data:

![](readmeImages/33.jpg)

Learning:

![](readmeImages/34.jpg)

Some examples:

![](readmeImages/35.jpg)

![](readmeImages/36.jpg)

![](readmeImages/37.jpg)

### 4.5 Self-made model

As mentioned above, we made 2 identical models ourselves. One used Keras and the other Fastai. We learned that Fastai is worthy of its name. I can't say that training in Kerase is dozens of times slower, but it is. By the time you repeatedly trained other Fastai models, including a similar self-made model, the Keras model reached 2.5 epochs within 11 hours.

Keras model:

![](readmeImages/38.jpg)

Analogous Fastai:

![](readmeImages/39.jpg)

The speed of Fastai is significantly greater.

The model architecture was selected based on an article about ImageNet and CNN. It was just a comparative test and we didn't plan to use it because it was clear that the pre-trained models were probably more powerful.

Keras:

![](readmeImages/40.jpg)

We did not wait for Keras model to finish training.

Trainign data of Fastai model:

![](readmeImages/41.jpg)

We can see that in terms of accuracy, the results do not  differ if the number of epoch is similar.


CNN in Fastai:

![](readmeImages/42.jpg)

Some examples:

![](readmeImages/43.jpg)

The reason could be that the function doesn't work the same way for self-made model since softmax outputs 38 predictions. We should probably look at the probability and set a threshold to determine the labels.

We made a separate function to get a similar set of random images with predicted labels.

Examples:

![](readmeImages/4_4_a.jpg)

![](readmeImages/4_4_b.jpg)

![](readmeImages/4_4_c.jpg)

As we can see, the model predict "linnaruum" for everything. For the model, the labels were so much out of balance, that the model found that it was best to always predict "linnaruum".

## 5. Classification

We used only two of the best models we got. In that way we can compare how differently the models classify the images.

We decided to retrain some models with 5 epoch , but the result showed that the models were overfitted.

Resnet34 with 5 epochs:

![](readmeImages/5_a.jpg)

Resnet50 with 5 epochs:

![](readmeImages/5_b.jpg)

Therefore, we stayed with our models with 3 epochs.

For classification, we give the model the same dataset that we used for training and see if it gives more labels to some images and compare the results.

![](readmeImages/5_1.jpg)

The results show that "linnaruum" having the more images affected out own self-made model the most. It classified everything as "linnaruum". We also picked out the 3 labels that had the highest predictions for every image. It shows that the labels start to vary from the second highest probability.

We classified 43601 images. If we look at how the predictions of different models overlapped, then the result is as follows:

![](readmeImages/5_2.jpg)

Classifications (how many images per label was classified).

Resnet34 classification:

![](readmeImages/5_3.jpg)

Resnet50 classification:

![](readmeImages/5_4.jpg)

Resnet152 classification:

![](readmeImages/5_5.jpg)

We can see that if the model was previously more trained then the predominance of "linnaruum" affected the results less. Resnet152 even predicted label "supilinn" that none of the other models predicted even once.

Our purpose was to give more labels to images and even out the labels coverage. That is why the indicators don’t show anything special since we gave the same training set to the model to get more labels. However, since usually accuracy is evaluated, the we did as well. Mostly the models predicted the label that was manually given to the image.

![](readmeImages/5_6.jpg)

We can see that Resnet152 does well compared to other models in giving the correct label to images. However, working with this model was slower than with others, even though Resnet50 took the longest to classify images.

Some examples of new labels given by Resnet152. The new additional labels are the 2nd and 3rd predicted labels. 

![](readmeImages/5_7.jpg)

![](readmeImages/5_8.jpg)

![](readmeImages/5_9.jpg)


## 6. Afterwards

Next, it is necessary to deal with where and how to store these images and make them searchable on the basis of a label or other information. Duplicates should not be used for storage. Finding pictures together should be as fast as possible.

The activities related to manual labels need to be improved. In other words, the images should be labeled more conservatively and then the models should be trained again.

Since we have an Imagenet-like dataset and the image title can always be regenerated from the hash, then we only need a relationship table to make changes to the labels and use them in training. There is no need to process images or perform any time-consuming operations on the original images.

If we find a way to store pictures and their data, search for pictures by keyword and we have new models, then we can hand over the work to the city government.

## 7. What we learned

The process of thi sproject gave us several llessons. We learned many things on the go and reading different materials. Some things we learned from eachother.

1. The first lesson is regarding the choice of topic. At first we chose one topic and then changet it since it didn't have enough machine learning components. The second topic that we chose is more process-oriented than result-oriented. The result is, of course, important as well, but nothing bad happens if some images get wrong labels. As a project, we could have done something where we could have strived for better result and then spend more effort on fine-tuning. It would have been more of competition with ourselves to see what is the best result we can get.
2. We found out that image processing is very time-consuming and inflexible activity. If a process is started then it is hard to repeat it in terms of time because every process took a vast amount of time. That is why we could not fix the thing we brought out as problems.
3. The images were large and it if a challenge to find a solution for storing them so that the search could be done reasonably fast. The goal is to make the image bank usable for people.
4. The image processing is time-consuming and needs more attention. We only gave the images a new shape and name. In terms of accuracy, we should have looked what rezising did with the image. We  didn't. Maybe it is also something we should have paid more attention to and make sure the images are understandable for the model.
5. Manually added labels are important. How well we give the initial labels affect the end result greatly. With our work, the labels might have been a bit too random and uncontrolled. The labels should have been tied to images based on a more complicated analysis.
6. Choosing labels is also complicated. If the label is too general then it will be hard to defferentiate images from eachother. That is why chooosing labels need to be done more carefully. We might do it again if we see that something should have been solved differently. Without knowing the database, it is hard to predict what will work and what won't. We got more familiar with the database only in the process of working.
7. A new approach of comparing images to find duplicates is to do it based on hashes. The more ususal way would have been to compare the image arrays. We solved the comparison and unique naming thanks to hashes. The names can be deduced from the arrays if you know what hash function is used. It means that if we take an original image from the dataset we can easily find its match in the modified database.
8. At first, Fastai seemed to not want to cooperate and was slow. In the end we found that Fastai is relatively fast. If Keras model trained 2.5 epochs in 11 hours, then Fastai took only 1-2 hours for the same model. Fastai is fast if it doesn't have to process the pictures during the training.
9. Using Fastai is a nuisance because the functions are a little different from usual. But if you google the right thing, you can probably find an answer. It allows you to use pre-trained models and create your own. You can use a wide variety of datasets. 

