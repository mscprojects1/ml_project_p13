from functools import reduce
from typing import Dict, Iterable, List, Set, Tuple, Union
import numpy as np

"""
paths_to_tokens on lihtsalt abifunk, ehk tarvilik ehk mitte


prepare_data võiks teoreetiliselt siis tagastada vajaliku et pmst treenima minna; y on feature matrix ja 
x on list piltide pathidest, encoder et pärast ümber convertida
põhimõtteliselt eeldab et data oleks kujul:
{'key': {'paths': {'a/b/c.jpg', 'y/e/e/t.png'}, 'labels': {'tartu', 'lapsed', 'eakad', 'jaan muna', ...},
 'key2': ...,
 'key3': ...}
kus key-d on selleks hetkeks tegelikult suva
pole aga vast keeruline ümber kirjutada kui teine struktuur tuleb, 'paths' andmetüüp võib selle funki jaoks ka suvaline
olla, antud hetkel lihtsalt eeldab et mingi konteiner

kõikvõimalikud labelid oleks tore ette sööta labels argumendina

pole testinud aga prolly töötab?
ok testisin ka, paistab toimiv
"""


def paths_to_tokens(paths_set: Iterable[str], sep: str = '/', lower: bool = True) -> Set[str]:
    """
    {'a/b/c', 'd/b/c'} ---> {'a', 'b', 'c', 'd'}

    :param paths_set: path-ide hulk
    :param sep: eraldaja
    :param lower: kas token lowercase-ida
    :return: hulk kus on kõik eraldatud "tokenid" antud pathidest
    """
    return reduce((lambda a, b: a | set(b.lower().split(sep))) if lower else (lambda a, b: a | set(b.split(sep))),
                  paths_set, set())


def prepare_data(labels: Union[List[str], Set[str]], data: Dict, sep='/') -> Tuple[List[str], np.ndarray, dict]:
    """
    Valmistab andmed ette töötluseks
    Siin proovisin läbi igasugused sklearn-i funkid aga ükski ei sobinud, üllatavalt
    seega custom encoder

    :param labels: kõik võimalikud labelid
    :param data: andmestik, kuju kirjeldatud üleval
    :param sep: eraldaja, igaks juhuks
    :return: X (list piltide pathidest), y (labelid), ümber pööratud "encoder"
                                                    (et pärast labelid taastada, selle võib faili dumpida)
    """
    encoder = {l: i for i, l in enumerate(labels)}  # lol, "encoder"
    X = []
    y = []
    for content in data.values():
        X.append(min(content['paths'], key=lambda x: x.count(sep)))  # lühim tee, mitte kriitiline aga nice to have
        labels = [0]*len(labels)
        for label in content['labels']:
            labels[encoder[label]] = 1
        y.append(labels)
    return X, np.array(y), {v: k for k, v in encoder.items()}

