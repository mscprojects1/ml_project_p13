#!/usr/bin/env python
# coding: utf-8

import os

names = set()
startpath = "./"
for root, dirs, files in os.walk(startpath):
    names.update(set(root))
    names.update(set(dirs))
    names.update(set(files))
    parents = str(root).split("/")[1:]
    names.update(set(parents))
    #for p in parents: 
        #names.add(str(p))
    #for f in files:
        #names.add(str(f))
with open('all_names.txt','w') as f:
    f.write(str(names))
