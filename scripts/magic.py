import os
import pickle
from hashlib import md5

SUPPORTED_EXTENSIONS = (
    'blp', 'bmp', 'dib', 'bufr', 'cur', 'pcx', 'dcx', 'dds', 'ps', 'eps', 'fit', 'fits', 'fli', 'flc', 'ftc', 'ftu',
    'gbr', 'gif', 'grib', 'h5', 'hdf', 'png', 'apng', 'jp2', 'j2k', 'jpc', 'jpf', 'jpx', 'j2c', 'icns', 'ico', 'im',
    'iim', 'tif', 'tiff', 'jfif', 'jpe', 'jpg', 'jpeg', 'mpg', 'mpeg', 'mpo', 'msp', 'palm', 'pcd', 'pdf', 'pxr', 'pbm',
    'pgm', 'ppm', 'pnm', 'psd', 'bw', 'rgb', 'rgba', 'sgi', 'ras', 'tga', 'icb', 'vda', 'vst', 'webp', 'wmf', 'emf',
    'xbm', 'xpm'
)

path = 'LVpildid'  # <------ piltide kausta juur

data = dict()

for dirpath, _, files in os.walk(path):
    for file in files:
        if file.lower().endswith(SUPPORTED_EXTENSIONS):
            filepath = f"{dirpath}{os.sep}{file}"
            key = md5(open(filepath, 'rb').read()).digest()
            value_set = data.setdefault(key, set())
            value_set.add(filepath)

with open('unikaalsed.bin', 'wb') as bf:
    pickle.dump(data, bf)
