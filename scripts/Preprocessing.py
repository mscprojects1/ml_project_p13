#!/usr/bin/env python
# coding: utf-8


from ZODB import FileStorage, DB
import transaction
from ZODB.PersistentMapping import PersistentMapping
from persistent import Persistent
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import hashlib
from PIL import Image
from pathlib import Path
from PIL.ExifTags import TAGS
import imghdr
import pandas as pd
from datetime import datetime
import time

# ## General help functions

def writetofile(filename, text):
    with open(filename, 'a', encoding='utf-8') as f:
        f.write(text)


def rowbyrow(filename):
    with open(filename, encoding='utf-8') as f:
        content = f.readlines()
    for i in range(len(content)):
        content[i] = content[i].strip()
    return content

def readfull(filename):
    with open(filename, encoding='utf-8') as f:
        content = f.read()
    return content

# ## Database for objects

storage = FileStorage.FileStorage('./scripts/db/TartuInPictures.fs')
db = DB(storage)
connection = db.open()
root = connection.root()

#root.items()

#if not "images" in root:
#    root["images"] = {}
#images=root["images"]

# ## Help functions for collecting image data

def getExif(img):
    exifdata = img.getexif()
    exif = {}
    for tag_id in exifdata:
        tag = TAGS.get(tag_id, tag_id)
        data = exifdata.get(tag_id)
        if isinstance(data, bytes):
            data = data.decode('utf-8', 'ignore')
        exif[tag] = data
    return exif

def getHash(img):
    img = np.array(img)
    img = img.copy(order='C')
    h = hashlib.new('ripemd160')
    h.update(img)
    return h.hexdigest()

def getLabel(path,labelword, authors):
    label = set()
    path = path.lower()
    author = ""
    for key, value in authors.items():
        if key in path:
            author = key
        for v in value:
            if v in path:
                author = key
    
    for key, value in labelword.items():
        if key in path and key not in author:
            label.add(key)
        for v in value:
            if v in path and v not in author:
                label.add(key)
                
    if author == "":
        author = None
    else:
        author = author.title()
    return list(label), author
 
# ## Image object

class TartuImage(Persistent):

    def __init__(self, ihash, exif, author=None, time=None, year=None, place=None, event=None):
        #self.image = image
        self.ihash = ihash
        self.path = []
        self.labels = []
        self.exif = exif
        self.author = author
        self.time = time
        self.year = year
        self.place = place
        self.event = event
        
    def out(self):
        print("Hash:", self.ihash)
        print("Path:", self.path)
        print("Labels:", self.labels)
        print("EXIF:", self.exif)
        print("Author:", self.author)
        print("Time:", self.time)
        print("Year:", self.year)
        print("Place:", self.place)
        print("Event:", self.event)


# ## Labels and relations

labels = rowbyrow("./scripts/labelsdata/labels.txt")
len(labels)

words = readfull("./scripts/labelsdata/labels_dict.txt")
obj_code = 'dict({})'.format(words)
labelword = eval(obj_code)

len(labelword)

#check if they are same
for key in labelword.keys():
    if key not in labels:
        print(key)

auth = readfull("./scripts/labelsdata/authors.txt")
obj_code = 'dict({})'.format(auth)
authors = eval(obj_code)

# ## Every label gets a folder

path = "./scripts/TartuNet/train/"
for l in labels:
    os.mkdir(path+l.strip())   

# ## Go through all images and fill database and make Imagenet style dataset

hashes = []
labelfile = pd.DataFrame(columns=["label","name","path"])

startpath = "./LVpildid"
ext = ["png", "jpg", "jpeg", "bmp", "pbm", "pgm", "ppm", "pxm"]
for r, dirs, files in os.walk(startpath):
    for f in files:
        path = os.path.join(r,f).replace("\\","/").strip()
        try:
        
            if imghdr.what(path) not in ext or imghdr.what(path) == None:
                continue
                                           
            im = Image.open(Path(path))
        
            #hash
            ihash = getHash(im)
        
            # if hash exists, it is a duplicate, only the path with information is saved
            if ihash in hashes:
                pilt = root[ihash]
                pilt.path += [path]
                pilt.labels += getLabel(path, labelword, authors)
                transaction.commit()
                continue
        
            hashes += [ihash]
        
            #read image and exif data
            exif = getExif(im)
            if "DateTimeOriginal" in exif and exif["DateTimeOriginal"] != "":
                time = exif["DateTimeOriginal"]
                year = exif["DateTimeOriginal"][:4]
        
            ilabels, author = getLabel(path, labelword, authors)
       
            # Make an object in class TartuImage        
            imgObject = TartuImage(ihash, exif)
            imgObject.time = time
            imgObject.year = year
            imgObject.author = author
            imgObject.path += [path]
            imgObject.labels += ilabels
        
            #Add the object to db
            #images[len(images)] = imgObject
            root[ihash] = imgObject
            transaction.commit()
        
            #ImageNet style dataset
            #preprocess and save
            ptodataset = "./scripts/TartuNet/train/"
            dim = (387, 469)
            if im.size[0] > im.size[1]:
                dim = (dim[1], dim[0])
                img_resized = im.resize(dim, Image.ANTIALIAS)
                for l in ilabels:
                    p = ptodataset + l +"/" + ihash + '.jpg'
                    if img_resized.mode in ("RGBA", "P"): 
                        img_resized = img_resized.convert("RGB")
                    img_resized.save(p, optimize=True, quality=95)
                    labelfile = labelfile.append({"label": l, "name": ihash + '.jpg', "path": p},ignore_index=True)
        except:
            print("Problem with:", path)
            continue

db.close()
storage.close()
labelfile.to_csv("./scripts/labelsdata/labeltofile.csv", sep=";")
